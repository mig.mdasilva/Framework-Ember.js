Objetivo: Aumentar a produtividade de desenvolvimento de aplicações web focando na ideia de que é mais vantajoso gastar algum tempo extra durante a criação desde que isso facilite o reparo e o manejo geral da aplicação posteriormente. 

Popularidade: A popularidade não é um ponto forte deste framework. Ember.js é um dos mais antigos frameworks JS, mas que ainda continua moderno e que, não somente corre atrás de boas ideias de outros frameworks, como também tem muitas de suas funcionalidades utilizadas pelos mesmos. Embora este não seja um framework muito comentado por entre os programadores, ele possui plenas condições de atender à muitas necessidades profissionais.

Requisitos: O único requisito é ter o Node e o npm instalado na maquina.

Documentação: A documentação do Ember encontra-se em seu site oficial: emberjs.com. 

Para a instalação do Ember, deve-se ter o Node instalado na máquina. 

Pode-se instalar o Ember com um único comando usando o npm, o gerenciador de pacotes Node.js. Digitando no seu terminal: 

npm install -g ember-cli 

Após instalado, você terá acesso a um novo Ember comando no seu terminal. Você pode usar o Ember new comando para criar um novo aplicativo. 

Esse comando criará um novo diretório chamado ember-quickstart e configurará um novo aplicativo Ember dentro dele. 

Ao fornecer tudo o que você precisa para criar aplicativos da web prontos para produção em um pacote integrado, o Ember torna fácil iniciar novos projetos. 

Vamos nos certificar de que tudo está funcionando corretamente. Cd no diretório do aplicativo ember-quickstart e inicie o servidor de desenvolvimento digitando: 

cd ember-quickstartember serve 

Depois de alguns segundos, você deve ver a saída que se parece com isso: 

Livereload server on http://localhost:7020Serving on http://localhost:4200/ 

(Para parar o servidor a qualquer momento, digite Ctrl-C no seu terminal.) 

Abra http://localhost:4200no seu navegador. Você deve ver uma página de boas-vindas da Ember com algumas poucas informações.

Parabéns! Você acabou de criar e inicializar seu primeiro aplicativo Ember. 

 Conclusão: 

Ember é um framework completo projetado para ser altamente opinativo. Ele fornece uma série de convenções estabelecidas e, uma vez que se fique bastante familiarizado com elas, pode se tornar muito produtivo. No entanto, também significa que a curva de aprendizado é elevada e a flexibilidade é sofrível. É uma escolha para se colocar na balança entre adotar um framework fortemente opinativo ou uma biblioteca com um conjunto de ferramentas de baixo acoplamento que funcionam em conjunto. Este último cenário proporciona mais liberdade, mas requer uma maior quantidade de decisões arquitetônicas por parte do usuário. 

Com isto dito, seria provavelmente mais adequado uma comparação entre o núcleo do Vue e as camadas de templates e de modelo de objetos do Ember: 

Vue oferece reatividade não obstrutiva em objetos JavaScript tradicionais e propriedades computadas totalmente automáticas. No Ember, você precisa envolver qualquer coisa em Objetos Ember e manualmente declarar dependências para propriedades computadas. 

A sintaxe de templates do Vue se arma com o poder total de expressões JavaScript, enquanto a sintaxe de expressões e helpers do Handlebars é bastante limitada. 

Em termos de performance, Vue supera Ember por uma margem justa, mesmo após a atualização mais recente do motor Glimmer no Ember 2.x. Vue realiza atualizações em lote automaticamente, enquanto no Ember você precisa lidar manualmente com laços de execução em situações de desempenho crítico.